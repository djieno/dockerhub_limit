FROM golang:1.15 AS build-env
WORKDIR /src/dockerhub-limit-exporter-go/src
# ADD src/go.* /src/dockerhub-limit-exporter-go/

ADD . /src/dockerhub-limit-exporter-go/
RUN go mod download
ENV CGO_ENABLED=0
RUN go build -o dockerhub-limit-exporter-go

FROM scratch
COPY --from=build-env /etc/ssl/certs /etc/ssl/certs
COPY --from=build-env /src/dockerhub-limit-exporter-go/src/dockerhub-limit-exporter-go /bin/dockerhub-limit-exporter-go
COPY --from=build-env /etc/passwd /etc/passwd
USER nobody
ENTRYPOINT ["/bin/dockerhub-limit-exporter-go"]